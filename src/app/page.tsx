import { useCarouselsQuery, useTabsQuery } from "@/generated/graphql";
import { serverFetch } from "@/libs";

export default async function Home() {
  const { carousels } = await serverFetch(useCarouselsQuery, {
    next: { revalidate: 15 },
  });

  return <main>{JSON.stringify(carousels)}</main>;
}
