import { FileFragment, Maybe } from "@/generated/graphql";

export function getStrapiURL(path = "") {
  return `${process.env.API_URL}${path}`;
}

export function getStrapiFile(file?: Maybe<FileFragment>) {
  if (file?.attributes?.url) {
    const url = file.attributes.url;
    return url.startsWith("/") ? getStrapiURL(url) : url;
  }
  return "";
}
