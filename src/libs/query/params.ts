import { BaseQuery } from "@/models";

export const defaultPage = 1;

export const defaultPageSize = 12;

export const defaultSort = "createdAt:desc";

export const defaultPagination = {
  page: defaultPage,
  pageSize: defaultPageSize,
};

export const initialPageParam = {
  pagination: defaultPagination,
};

export function getNextPageParam<T extends BaseQuery>(data: T) {
  const key = Object.keys(data).filter((key) => key != "__typename")[0];

  const { page = defaultPage, pageCount = 1 } = data[key]["meta"]["pagination"];

  if (page < pageCount) {
    return {
      pagination: {
        page: page + 1,
        pageSize: defaultPageSize,
      },
    };
  }
}

export const infiniteQueryOptions = {
  initialPageParam,
  getNextPageParam,
};
