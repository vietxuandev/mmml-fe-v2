import type { CodegenConfig } from "@graphql-codegen/cli";

const config: CodegenConfig = {
  overwrite: true,
  schema: "http://localhost:1337/graphql",
  documents: "src/**/*.graphql",
  generates: {
    "src/generated/graphql.ts": {
      plugins: [
        {
          add: {
            content: `import { GraphQLFileUpload } from '@/models';`,
          },
        },
        {
          add: {
            content: `import { RequestInit } from '@/libs';`,
          },
        },
        "typescript",
        "typescript-operations",
        "typescript-react-query",
      ],
      config: {
        fetcher: {
          func: "@/libs#fetcher",
        },
        scalars: {
          DateTime: "string",
          JSON: "Record<string, unknown>",
          Upload: "Promise<GraphQLFileUpload>",
        },
        addInfiniteQuery: true,
        reactQueryVersion: 5,
        exposeQueryKeys: true,
        exposeFetcher: true,
      },
    },
  },
};

export default config;
